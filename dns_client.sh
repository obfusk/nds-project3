#!/bin/bash
args=( "$@" ) n=$#
host="${args[$((n-1))]}"
unset args[$((n-1))]
python dns.py -l "$host" "${args[@]}"
